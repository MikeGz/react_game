import React from "react";
import "./homepage.css";
import { ReactComponent as Triangle } from "../../assets/img/bg-triangle.svg";
import Token from "../../components/tokens/token.component";

function App() {
  const moves = [
    {
      type: "paper",
    },
    {
      type: "scissors",
    },
    {
      type: "rock",
    },
  ];

  return (
    <div className="App">
      <div className="game-area">
        {moves.map(({ type }, index) => (
          <Token key={index} type={type} />
        ))}
        <Triangle />
      </div>
    </div>
  );
}

export default App;
