import React from "react";
import Token from "../../components/tokens/token.component";
import "./gamepage.style.css";
import { useSelector, useDispatch } from "react-redux";
import { incrementScore, incrementScoreCpu } from "../../features/game/gameSlice";
import { Link } from "react-router-dom";

function Game() {
  const moves = ["rock", "paper", "scissors"];
  const cpuChoice = Math.floor(Math.random() * moves.length);
  const cpuMove = moves[cpuChoice];
  const playerMove = useSelector((state) => state.game.move);
  const dispatch = useDispatch();
  let result = "";

  //mettre un bouton pour retourner à la page précédente
  if (
    (playerMove === "rock" && cpuMove === "paper") ||
    (playerMove === "paper" && cpuMove === "scissors") ||
    (playerMove === "scissors" && cpuMove === "rock")
  ) {
    result = "You Lose";
    dispatch(incrementScoreCpu());
  } else if (
    (cpuMove === "rock" && playerMove === "paper") ||
    (cpuMove === "paper" && playerMove === "scissors") ||
    (cpuMove === "scissors" && playerMove === "rock")
  ) {
    result = "You Win";
    dispatch(incrementScore());
  } else if (playerMove === cpuMove) {
    result = "Draw";
  }
  return (
    <div className="Game">
      <div className="fight-zone">
        <p className="insights">YOU</p>
        <p className="insights">CPU</p>
        <div className="token-choice">
          <Token type={playerMove} />
          <p className="result">{result} !!!</p>
          <Token type={cpuMove} />
        </div>
        <Link className="back-button" to="/">
          New Game
        </Link>
      </div>
    </div>
  );
}

export default Game;
