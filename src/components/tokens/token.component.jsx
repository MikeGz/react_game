import React from "react";
import "./token.style.css";
import { useNavigate } from "react-router-dom";
import { ReactComponent as Paper } from "../../assets/img/icon-paper.svg";
import { ReactComponent as Rock } from "../../assets/img/icon-rock.svg";
import { ReactComponent as Scissors } from "../../assets/img/icon-scissors.svg";
import { useDispatch } from "react-redux";
import { changeMove } from "../../features/game/gameSlice";

function Token({ type }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();


  const playerMove = () => {
    dispatch(changeMove(type));
    navigate("/gamepage");
  };

  return (
    <div className={`token-container ${type}`} onClick={playerMove}>
      {(() => {
        switch (type) {
          case "paper":
            return <Paper />;
          case "rock":
            return <Rock />;
          case "scissors":
            return <Scissors />;
          default:
            return;
        }
      })()}
    </div>
  );
}

export default Token;
