import React from "react";
import { useSelector } from "react-redux";
import "./score.style.css";
import {ReactComponent as HeaderLogo} from '../../assets/img/logo.svg'; 


export function Score() {
  const scorePlayer = useSelector((state) => state.game.score);
  const scoreCpu = useSelector((state) => state.game.scoreCpu);

  return (
    <div className="score-container">
      <div className="score">
        <h3>YOU</h3>
        <p className="value">{scorePlayer}</p>
      </div>
      <HeaderLogo />
      <div className="score">
        <h3>CPU</h3>
        <p className="value">{scoreCpu}</p>
      </div>
    </div>
  );
}
