import { createSlice } from '@reduxjs/toolkit'

export const gameSlice = createSlice({
  name: 'game',
  initialState: {
    score: 0,
    scoreCpu: 0,
    move: ''
  },
  reducers: {
    incrementScore: (state) => {
      state.score += 1
    },
    changeMove: (state, action) => {
      state.move = action.payload
    },
    incrementScoreCpu: (state) => {
      state.scoreCpu += 1
    },
  },
})

// Action creators are generated for each case reducer function
export const { incrementScore, changeMove, incrementScoreCpu} = gameSlice.actions

export default gameSlice.reducer